# dotnet8.0

The dotnet8.0 package

This is the .NET 8.0 package for RHEL/CentOS Stream.

Please report any issues [using Jira](https://issues.redhat.com) in the RHEL
project against `dotnet8.0` component.

# Specification

This package follows [package naming and contents suggested by
upstream](https://docs.microsoft.com/en-us/dotnet/core/build/distribution-packaging),
with one exception. It installs dotnet to `/usr/lib64/dotnet` (aka
`%{_libdir}`).

# Contributing

1. Fork the repo.

2. Checkout the forked repository.

    - `git clone git@gitlab.com:$USER/centos_rpms_dotnet8.0.git dotnet8.0`
    - `cd dotnet8.0`

3. Make your changes. Don't forget to add a changelog.

   If you are updating to a new upstream release: Get the new upstream source
   tarball and the detached signature. Update the versions in the spec file.
   Add a changelog. This is generally automated by the following.

    - `./update-release <sdk-version> <runtime-version>`

    If this fails because of compiler errors, you might have to figure
    out a fix, then add the patch in `build-dotnet-tarball` script
    rather than the spec file.

4. Do local builds.

    - `centpkg local`

5. Fix any errors that come up and rebuild until it works locally. Any
   patches that are needed at this point should be added to the spec file.

6. Do builds in koji.

    - `centpkg scratch-build --srpm`

7. If this is a new release, upload the source archive and dtached signature to
   the look-aside cache.

    - `centpkg new-sources dotnet-source-tarball.tar.gz dotnet-source-tarball.tar.gz.sig`

8. Commit the changes to the git repo.

    - `git add` any new patches
    - `git remove` any now-unnecessary patches
    - `git commit -a`
    - `git push`

9. Create a pull request with your changes.

10. Once the tests in the pull-request pass, and reviewers are happy, do a real
    build.

    - `centpkg build`

# Testing

This package uses CI tests as defined in `tests/ci.fmf`. Creating a
pull-request or running a build will fire off tests and flag any issues. We
have enabled gating (via `gating.yaml`) on the tests. That prevents a build
that fails any test from being released until the failures are waived.

The tests themselves are contained in this external repository:
https://github.com/redhat-developer/dotnet-regular-tests/
